
#if defined(ESP8266)
#include <pgmspace.h>
#else
#include <avr/pgmspace.h>
#endif

/* for software wire use below
#include <SoftwareWire.h>  // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>

SoftwareWire myWire(SDA, SCL);
RtcDS3231<SoftwareWire> Rtc(myWire);
 for software wire use above */

/* for normal hardware wire use below */
#include <Wire.h> // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>
#include <SoftwareSerial.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <RingBufCPP.h>

#include <Ticker.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>



const char* ssid = "SMART ENGAGE HUB";
const char* passphrase = "asdfgh1234";
String serialNum = "59017f22d1731";
String esid;
String epass = "";
String st;
String content;

//button handling vars
int current;         // Current state of the button
long millis_held;    // How long the button was held (milliseconds)
long secs_held;      // How long the button was held (seconds)
long prev_secs_held; // How long the button was held in the previous check
bool previous = HIGH;
unsigned long firstTime; // how long since the button was first pressed 



Ticker flipper, pinger;

#define buttonPin 12
#define MAX_NUM_ELEMENTS_MQTT 100
#define MAX_NUM_ELEMENTS 50

/* for normal hardware wire use above */
//Software Serial1 RX pin D5

RtcDS3231<TwoWire> Rtc(Wire);
ESP8266WebServer server(80);

//SoftwareSerial1 Serial(14, 12, false, 256);


struct Event
{
  int sensorID;
  bool state;
  unsigned long timestamp;
};

RingBufCPP<struct Event, MAX_NUM_ELEMENTS_MQTT> mqttBuf;
RingBufCPP<struct Event, MAX_NUM_ELEMENTS> tempBuf;


unsigned long lastRTCUpdate = 0,lastRTCUpdateMillis = 0, lastMQTTRequest = 0,ButtonPressedTime = 0, lastPing = 0;


char* ssid_router = "CYRUP LINK";
char* password = "cyrupqaz";

char* topic = "test";
//char* server = "m20.cloudmqtt.com";
char* server2 = "se-hub-mqtt-server.sb.cyrupdev.net";
char* username = "qvtygpxz";
char* pass = "Z08xFp-PzpFj";

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete



void callback(char* topic, byte* payload, unsigned int length);
String macToStr(const uint8_t* mac);
bool publish(String payload);
void connectToWiFi();
long getTimeStamp();
void serialEventRun(void);
void createWebServer(int webtype);
void setupAP(void);
bool testWifi(void);
void launchWeb(int webtype);
void sendPing();

WiFiClient wifiClient;

//15858
PubSubClient client(server2, 1883, callback, wifiClient);


String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

void connectToWiFi(){
  Serial1.print("Connecting to ");
  Serial1.println(esid);
  
  //WiFi.begin(ssid_router, password);
  WiFi.begin(esid.c_str(), epass.c_str());
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial1.print(".");
    handleButtonPress();
    //serialEventRun();
  }
  Serial1.println("");
  Serial1.println("WiFi connected");  
  Serial1.println("IP address: ");
  Serial1.println(WiFi.localIP());

  // Generate client name based on MAC address and last 8 bits of microsecond counter

}

void connectMQTT(){

  String clientName;
  clientName += "esp8266-";
  uint8_t mac[6];
  WiFi.macAddress(mac);
  clientName += macToStr(mac);
  clientName += "-";
  clientName += String(micros() & 0xff, 16);

  Serial1.print("Connecting to broker");
  Serial1.print(" as ");
  Serial1.println(clientName);

  bool mqttConnected = false;
  int counter = 0;

  while(!mqttConnected){
    //if (client.connect("cyrup","qvtygpxz","Z08xFp-PzpFj")) {
     if(WiFi.status() != WL_CONNECTED){
        connectToWiFi();
      }
    //serialEventRun();  
    if((millis() - lastMQTTRequest) > 5000){
      lastMQTTRequest = millis();
      if (client.connect("SN59017f22d1731","chanaka","asdfgh123")) {
        //serialEventRun();  
        Serial1.println("Connected to MQTT broker");
        Serial1.print("Topic is: ");
        Serial1.println(topic);
        
        if (client.publish(topic, "$H,59017f22d1731$")) {
          Serial1.println("Publish ok");
          mqttConnected = 1;
        }
        else {
          Serial1.println("Publish failed");
        }
      }
      else {
        Serial1.println("MQTT connect failed");
        Serial1.println("trying again...");
        Serial1.println(lastMQTTRequest);
        
        counter++;
        if(counter > 50){
          Serial1.println("Aborting...");
          abort();
        }
      }
    }
}

}

void handleButtonPress(){
  current = digitalRead(buttonPin);

  if(current){
    firstTime = millis();
    }else{
      if((millis() - firstTime) > 3000){
        setupAP();
       while(1){
          server.handleClient();
    
        }
       }
    }
 
}

void sendPing(){
  if((millis() - lastPing) > 300000){
  String ping = "$H,59017f22d1731$";
  publish(ping);
  lastPing = millis();
  }

}

bool publish(String payload){
  bool dataSent = 0;
  int counter = 0;
  while(!dataSent && counter <= 3){
    if (client.connected()){
      Serial1.print("Sending payload: ");
      Serial1.println(payload);
      if (client.publish(topic, (char*) payload.c_str())) {
        Serial1.println("Publish ok");
        dataSent = 1;
        return true;
      }
      else {
        Serial1.println("Publish failed");
        //serialEventRun();
        if(counter > 1){
          client.disconnect();
          connectMQTT();
        }
      }
    }
    else{
      if(WiFi.status() != WL_CONNECTED){
        connectToWiFi();
        connectMQTT();
      }
      else{
        connectMQTT();
      }
    }
    counter++;
  }
}



void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
}


long getTimeStamp(){
  if(millis() - lastRTCUpdateMillis < 500)
    return lastRTCUpdate;
  else if (Rtc.IsDateTimeValid()){
    RtcDateTime now = Rtc.GetDateTime();
    lastRTCUpdate = now.TotalSeconds();  
    lastRTCUpdateMillis = millis();
    return lastRTCUpdate;
  }
  else{
    return 0;
    }
  }




//siba modaya
void setup() {
  
  Serial.begin(115200);
  Serial.swap();
  Serial1.begin(115200);
  pinMode(buttonPin, INPUT_PULLUP);

  EEPROM.begin(512);
  delay(10);
  Serial1.println();
  Serial1.println();
  Serial1.println("Startup");
  // read eeprom for ssid and pass
  Serial1.println("Reading EEPROM ssid");

  for (int i = 0; i < 32; ++i)
    {
      esid += char(EEPROM.read(i));
    }
  Serial1.print("SSID: ");
  Serial1.println(esid);
  Serial1.println("Reading EEPROM pass");
  
  for (int i = 32; i < 96; ++i)
    {
      epass += char(EEPROM.read(i));
    }
  Serial1.print("PASS: ");
  Serial1.println(epass);  

  connectToWiFi();
  connectMQTT();
  Rtc.Begin();
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); 
  getTimeStamp();
  inputString.reserve(50);

  Serial1.println("\nSoftware Serial1 test started");
Serial1.println(getTimeStamp());
  for (char ch = ' '; ch <= 'z'; ch++) {
    Serial.write(ch);
  }
  Serial.println("");
  flipper.attach(0.1, serialEventRun);
 
}


struct Event temp;
struct Event temp2;
void loop() {
    handleButtonPress();
    if(mqttBuf.numElements()>0){
        while (mqttBuf.pull(&temp2))
        {
          //
          String payload = "$T," + String(temp2.sensorID) + "," + String(temp2.state) + "," + String(temp2.timestamp) + "," + serialNum + "$";
          Serial1.print("payload: ");
          Serial1.println(payload);
      
          publish(payload);
          //serialEventRun();
          Serial1.println();
        }
    }


    client.loop();
    sendPing();
   //serialEventRun();

}


void serialEventRun(void) {
  
     while (Serial.available() > 0) {
    
      // get the new byte:
    char inChar = (char)Serial.read();
    if (inChar == 'U' || inChar == 'D'){
      if(inChar == 'U' ){
         temp.state = 1;
        }else
          temp.state = 0;
      inChar = (char)Serial.read();
      while(inChar != '0'){
        inChar = (char)Serial.read();
      }
      inputString = "";
    }
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      temp.sensorID = inputString.toInt();
      temp.timestamp = getTimeStamp();

      //add to the FIFO
      for(int i = 0; i < tempBuf.numElements(); i++){
           Event *e = tempBuf.peek(i);
           if((temp.timestamp - (*e).timestamp) > 5){
              tempBuf.pull(&temp2);
              }
      }
      if (tempBuf.isEmpty()){
        mqttBuf.add(temp);
        tempBuf.add(temp);
        }
      else if (!tempBuf.isFull())
      {
        bool isUnique = 1;
        for(int i = 0; i < tempBuf.numElements(); i++){
           Event *e = tempBuf.peek(i);
           if((*e).sensorID == temp.sensorID && temp.state == (*e).state){
              isUnique = 0;
            }
        }
        if(isUnique){
          mqttBuf.add(temp);
          tempBuf.add(temp);  
        }
      }
         
      inputString = "";
    }   
  }

}


void createWebServer(int webtype)
{

  if ( webtype == 1 ) {
    server.on("/", []() {
        IPAddress ip = WiFi.softAPIP();
        String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        content = "<!DOCTYPE HTML>\r\n<html>Smart Engage Configuration ";
        content += ipStr;
        content += "<p>";
        content += st;
        content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
        content += "</html>";
        server.send(200, "text/html", content);  
    });
    server.on("/setting", []() {
        String qsid = server.arg("ssid");
        String qpass = server.arg("pass");
        if (qsid.length() > 0 && qpass.length() > 0) {
          Serial1.println("clearing eeprom");
          for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
          Serial1.println(qsid);
          Serial1.println("");
          Serial1.println(qpass);
          Serial1.println("");

          Serial1.println("writing eeprom ssid:");
          for (int i = 0; i < qsid.length(); ++i)
            {
              EEPROM.write(i, qsid[i]);
              Serial1.print("Wrote: ");
              Serial1.println(qsid[i]); 
            }
          Serial1.println("writing eeprom pass:"); 
          for (int i = 0; i < qpass.length(); ++i)
            {
              EEPROM.write(32+i, qpass[i]);
              Serial1.print("Wrote: ");
              Serial1.println(qpass[i]); 
            }    
          EEPROM.commit();
          content = "<!DOCTYPE HTML>\r\n<html>";
          content += "<p>Credentials Saved... Please reboot</p></html>";
        } else {
          content = "Error";
          Serial1.println("Sending 404");
        }
        server.send(200, "text/html", content);
    });
  } else if (webtype == 0) {
    server.on("/", []() {
      server.send(200, "text/plain", "this works as well");
    });
    server.on("/setting", []() {
      server.send(200, "text/plain", "setting.");
    });
    server.on("/cleareeprom", []() {
      content = "<!DOCTYPE HTML>\r\n<html>";
      content += "<p>Clearing the EEPROM</p></html>";
      server.send(200, "text/html", content);
      Serial1.println("clearing eeprom");
      for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
      EEPROM.commit();
    });
  }
}

void setupAP(void) {
  WiFi.mode(WIFI_AP);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial1.println("scan done");
  if (n == 0)
    Serial1.println("no networks found");
  else
  {
    Serial1.print(n);
    Serial1.println(" networks found");
    for (int i = 0; i < n; ++i)
     {
      // Print SSID and RSSI for each network found
      Serial1.print(i + 1);
      Serial1.print(": ");
      Serial1.print(WiFi.SSID(i));
      Serial1.print(" (");
      Serial1.print(WiFi.RSSI(i));
      Serial1.print(")");
      Serial1.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
      delay(10);
     }
  }
  Serial1.println(""); 
  st = "<ol>";
  for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*";
      st += "</li>";
    }
  st += "</ol>";
  delay(100);
  WiFi.softAP(ssid, passphrase, 6);
  Serial1.println("softap");
  launchWeb(1);
  Serial1.println("over");
}

bool testWifi(void) {
  int c = 0;
  Serial1.println("Waiting for Wifi to connect");  
  while ( c < 20 ) {
    if (WiFi.status() == WL_CONNECTED) { return true; } 
    delay(500);
    Serial1.print(WiFi.status());    
    c++;
  }
  Serial1.println("");
  Serial1.println("Connect timed out, opening AP");
  return false;
} 

void launchWeb(int webtype) {
  Serial1.println("");
  Serial1.println("WiFi connected");
  Serial1.print("Local IP: ");
  Serial1.println(WiFi.localIP());
  Serial1.print("SoftAP IP: ");
  Serial1.println(WiFi.softAPIP());
  createWebServer(webtype);
  // Start the server
  server.begin();
  Serial1.println("Server started"); 
}